﻿namespace ShoppingCart;

public class ShoppingCart : IShoppingCart
{
    public Guid Id { get; }
    public DateTime CreatedDate { get; }
    public DateTime UpdatedDate { get; set; }

    public ShoppingCart()
    {
        Id = Guid.NewGuid();
        CreatedDate = DateTime.Now;
        UpdatedDate = DateTime.Now;
    }
}