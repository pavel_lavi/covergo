﻿public interface IShoppingCart
{
    public Guid Id { get; }
    public DateTime CreatedDate { get; }
    public DateTime UpdatedDate { get; set; }

}