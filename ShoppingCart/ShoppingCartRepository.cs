﻿namespace ShoppingCart;

public class ShoppingCartRepository : IShoppingCartRepository
{
    private List<ShoppingCartItem> _shoppingCartItems;

    public decimal ShoppingCartTotal { get; private set; }

    public ShoppingCartRepository()
    {
        _shoppingCartItems = new List<ShoppingCartItem>();
        ShoppingCartTotal = 0;
    }

    public void UpdateShoppingCartTotal()
    {
        ShoppingCartTotal = _shoppingCartItems.Sum(x => x.Price);
    }

    public IEnumerable<ShoppingCartItem> GetShoppingCartItems()
    {
        return _shoppingCartItems;
    }

    public ShoppingCartItem GetShoppingCartItem(Guid id)
    {
        try
        {
            return _shoppingCartItems.First(x => x.Id == id);
        }
        catch (Exception e)
        {
            throw new Exception($"ShoppingCartItem with id {id} not found", e);
        }
    }

    public void AddShoppingCartItem(ShoppingCartItem productItem)
    {
        if (_shoppingCartItems.Any(x => x.ProductId == productItem.Id))
        {
            UpdateShoppingCartItem(productItem);
        }
        else
        {
            _shoppingCartItems.Add(productItem);
        }

        UpdateShoppingCartTotal();
    }

    public void DeleteShoppingCartItem(Guid id)
    {
        if (_shoppingCartItems.Any(x => x.Id == id))
        {
            _shoppingCartItems.Remove(GetShoppingCartItem(id));
            UpdateShoppingCartTotal();
        }
    }

    public void UpdateShoppingCartItem(ShoppingCartItem shoppingCartItem)
    {
        DeleteShoppingCartItem(shoppingCartItem.Id);
        _shoppingCartItems.Add(shoppingCartItem);
        UpdateShoppingCartTotal();
    }
}