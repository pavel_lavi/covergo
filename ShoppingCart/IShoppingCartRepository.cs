﻿public interface IShoppingCartRepository
{
    public decimal ShoppingCartTotal { get; }
    IEnumerable<ShoppingCartItem> GetShoppingCartItems();
    ShoppingCartItem GetShoppingCartItem(Guid id);
    void AddShoppingCartItem(ShoppingCartItem shoppingCartItem);
    void DeleteShoppingCartItem(Guid id);
    void UpdateShoppingCartItem(ShoppingCartItem shoppingCartItem);
    void UpdateShoppingCartTotal();

}