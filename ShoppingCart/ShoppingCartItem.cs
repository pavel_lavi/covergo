﻿public class ShoppingCartItem
{
    private int _quantity;

    public Guid Id { get;}
    public Guid ProductId { get; }
    public int Quantity
    {
        get => _quantity;
        set
        {
            _quantity = value;
            UpdatePrice();
        }
    }
    public decimal Price { get; private set; }

    public ShoppingCartItem(Guid productId, int quantity, decimal price)
    {
        Id = Guid.NewGuid();
        ProductId = productId;
        Quantity = quantity;
        Price = price;
    }

    private void UpdatePrice()
    {
        Price = Quantity * Price;
    }
}