﻿namespace ShoppingCart;

public class ShoppingCartAggregate
{
    private IShoppingCartRepository _shoppingCartRepository;
    private IShoppingCart _shoppingCart;

    public ShoppingCartAggregate(IShoppingCart shoppingCart, IShoppingCartRepository shoppingCartRepository)
    {
        _shoppingCartRepository = shoppingCartRepository;
        _shoppingCart = shoppingCart;
    }

    public Guid Id => _shoppingCart.Id;
    public DateTime CreatedDate => _shoppingCart.CreatedDate;
    public DateTime UpdatedDate => _shoppingCart.UpdatedDate;

    public decimal ShoppingCartTotal => _shoppingCartRepository.ShoppingCartTotal;

    public void AddShoppingCartItem(ShoppingCartItem product)
    {
        _shoppingCartRepository.AddShoppingCartItem(product);
        UpdateShoppingCartChangedDate();
    }

    public void DeleteShoppingCartItem(Guid id)
    {
        _shoppingCartRepository.DeleteShoppingCartItem(id);
        UpdateShoppingCartChangedDate();
    }

    public void UpdateShoppingCartItem(ShoppingCartItem shoppingCartItem)
    {
        _shoppingCartRepository.UpdateShoppingCartItem(shoppingCartItem);
        UpdateShoppingCartChangedDate();
    }

    public IEnumerable<ShoppingCartItem> GetShoppingCartItems()
    {
        return _shoppingCartRepository.GetShoppingCartItems();
    }

    private void UpdateShoppingCartChangedDate()
    {
        _shoppingCart.UpdatedDate = DateTime.Now;
    }
}