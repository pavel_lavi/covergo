﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using ShoppingCart;

namespace ShoppingCartTests;

public class ShoppingCartAggregateTests
{
    [Fact]
    public void AddShoppingCartItem_ShouldAddShoppingCartItem()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var productItem = fixture.Create<ShoppingCartItem>();
        var shoppingCartRepositoryMock = fixture.Freeze<Mock<IShoppingCartRepository>>();
        var shoppingCart = fixture.Create<ShoppingCartAggregate>();
        //act
        shoppingCart.AddShoppingCartItem(productItem);
        //assert
        shoppingCartRepositoryMock.Verify(x => x.AddShoppingCartItem(productItem), Times.Once);
    }

    [Fact]
    public void DeleteShoppingCartItem_ShouldDeleteShoppingCartItem()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var id = fixture.Create<Guid>();
        var shoppingCartRepositoryMock = fixture.Freeze<Mock<IShoppingCartRepository>>();
        var shoppingCart = fixture.Create<ShoppingCartAggregate>();
        //act
        shoppingCart.DeleteShoppingCartItem(id);
        //assert
        shoppingCartRepositoryMock.Verify(x => x.DeleteShoppingCartItem(id), Times.Once);
    }

    [Fact]
    public void AddShoppingCartItem_ShouldUpdateShoppingCartDate()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var shoppingCartItem = fixture.Create<ShoppingCartItem>();
        var shoppingCartMock = new ShoppingCart.ShoppingCart();
        var shoppingCartRepositoryMock = fixture.Freeze<Mock<IShoppingCartRepository>>();
        var shoppingCart = new ShoppingCartAggregate(shoppingCartMock, shoppingCartRepositoryMock.Object);
        //act
        shoppingCart.AddShoppingCartItem(shoppingCartItem);
        //assert
        shoppingCartMock.UpdatedDate.Should().BeCloseTo(DateTime.Now, TimeSpan.FromMinutes(1));
    }


    [Fact]
    public void AddShoppingCartItem_ShouldReturenOnGet()
    {

        //todo: get

        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var productItem = fixture.Create<ShoppingCartItem>();
        var shoppingCartRepositoryMock = fixture.Freeze<Mock<IShoppingCartRepository>>();
        var shoppingCart = fixture.Create<ShoppingCartAggregate>();
        //act
        shoppingCart.AddShoppingCartItem(productItem);
        //assert
        shoppingCartRepositoryMock.Verify(x => x.AddShoppingCartItem(productItem), Times.Once);
    }

    [Fact]
    public void AddShoppingCartItem_ShouldBeReturenOnGet()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var productItem = fixture.Create<ShoppingCartItem>();
        var shoppingCartRepositoryMock = fixture.Freeze<Mock<IShoppingCartRepository>>();
        var shoppingCart = fixture.Create<ShoppingCartAggregate>();
        //act
        shoppingCart.AddShoppingCartItem(productItem);
        //assert
        shoppingCartRepositoryMock.Verify(x => x.AddShoppingCartItem(productItem), Times.Once);
    }

}