﻿using FluentAssertions;

namespace ShoppingCartTests
{
    public class ShoppingCartItemTests
    {
        [Fact]
        public void TotalShoppingCarPriceQuantity_ShouldReturnTotalShoppingCartPrice()
        {
            //arrange
            var singleItemPrice = 10;
            var productItem = new ShoppingCartItem(Guid.NewGuid(), 1, singleItemPrice);
            //act
            var totalQuantity = 2;
            productItem.Quantity = totalQuantity;

            //assert
            productItem.Price.Should().Be(totalQuantity * singleItemPrice);
        }

    }
}
