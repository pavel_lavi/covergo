﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using ShoppingCart;

namespace ShoppingCartTests;

public class ShoppingCartRepositoryTests
{
    [Fact]
    public void AddShoppingCartItem_ShouldAddShoppingCartItem()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var productItem = fixture.Create<ShoppingCartItem>();
        var shoppingCartRepository = new ShoppingCartRepository();
        //act
        shoppingCartRepository.AddShoppingCartItem(productItem);
        //assert
        shoppingCartRepository.GetShoppingCartItems().Should().Contain(productItem);
    }

    [Fact]
    public void DeleteShoppingCartItem_ShouldDeleteShoppingCartItem()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var productItem = fixture.Create<ShoppingCartItem>();
        var shoppingCartRepository = fixture.Create<ShoppingCartRepository>();
        //act
        shoppingCartRepository.AddShoppingCartItem(productItem);
        shoppingCartRepository.DeleteShoppingCartItem(productItem.Id);
        //assert
        shoppingCartRepository.GetShoppingCartItems().Should().NotContain(productItem);
    }

    [Fact]
    public void UpdateShoppingCartItem_ShouldUpdateShoppingCartItem()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var productItem = fixture.Create<ShoppingCartItem>();
        var shoppingCartRepository = fixture.Create<ShoppingCartRepository>();
        //act
        shoppingCartRepository.AddShoppingCartItem(productItem);
        productItem.Quantity = 100;
        shoppingCartRepository.UpdateShoppingCartItem(productItem);
        //assert
        shoppingCartRepository.GetShoppingCartItems().Should().Contain(productItem);
        shoppingCartRepository.GetShoppingCartItems().Should().HaveCount(1);
    }

    [Fact]
    public void GetShoppingCartItems_ShouldReturnShoppingCartItems()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var shoppingCartRepository = fixture.Create<ShoppingCartRepository>();
        //act
        var shoppingCartItems = shoppingCartRepository.GetShoppingCartItems();
        //assert
        shoppingCartItems.Should().NotBeNull();
    }

    [Fact]
    public void GetShoppingCartItems_ShouldReturnEmptyShoppingCartItems()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var shoppingCartRepository = fixture.Create<ShoppingCartRepository>();
        //act
        var shoppingCartItems = shoppingCartRepository.GetShoppingCartItems();
        //assert
        shoppingCartItems.Should().BeEmpty();
    }

    [Fact]
    public void TotalShoppingCarPrice_ShouldReturnTotalShoppingCartPrice()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var productItem = fixture.Create<ShoppingCartItem>();
        var shoppingCartRepository = fixture.Create<ShoppingCartRepository>();
        //act
        shoppingCartRepository.AddShoppingCartItem(productItem);
        //assert
        shoppingCartRepository.ShoppingCartTotal.Should().Be(productItem.Price);
    }

    [Fact]
    public void AddTwoShoppingCartItems_TotalPriceShouldBeBothItems()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var productItem = fixture.Create<ShoppingCartItem>();
        var productItem2 = fixture.Create<ShoppingCartItem>();
        var shoppingCartRepository = fixture.Create<ShoppingCartRepository>();
        //act
        shoppingCartRepository.AddShoppingCartItem(productItem);
        shoppingCartRepository.AddShoppingCartItem(productItem2);
        //assert
        shoppingCartRepository.ShoppingCartTotal.Should().Be(productItem.Price + productItem2.Price);
    }

    [Fact]
    public void UpdateShoppingCartItem_ShouldUpdateTotalPrice()
    {
        //arrange
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        var productItem = fixture.Create<ShoppingCartItem>();
        var shoppingCartRepository = fixture.Create<ShoppingCartRepository>();
        //act
        shoppingCartRepository.AddShoppingCartItem(productItem);
        productItem.Quantity = 100;
        shoppingCartRepository.UpdateShoppingCartItem(productItem);
        //assert
        shoppingCartRepository.ShoppingCartTotal.Should().Be(productItem.Price);
    }

}